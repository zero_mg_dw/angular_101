import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  myTitle = 'desarrollo01';

  constructor(private router: Router) {

  }

  private testString(): string {
    return '';
  }

  public routerToTest01(): void {
    this.router.navigate(['/path01']);
  }

  public routerToTest02(): void {
    this.router.navigate(['/path02']);
  }


}
