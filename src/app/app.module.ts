import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { Test01Component } from './test01/test01.component';
import { Test02Component } from './test02/test02.component';

@NgModule({
  declarations: [
    AppComponent,
    Test01Component,
    Test02Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
