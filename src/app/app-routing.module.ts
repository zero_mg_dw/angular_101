import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Test01Component} from './test01/test01.component';
import {Test02Component} from './test02/test02.component';
import {AppComponent} from './app.component';

const routes: Routes = [
  {
    path: 'path01',
    component: Test01Component,
  },
  {
    path: 'path02',
    component: Test02Component,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
